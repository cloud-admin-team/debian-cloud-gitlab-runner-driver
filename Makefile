all: gitlab-runner podman-remote

gitlab-runner:
	podman run -t --rm --volume $(PWD):/app --workdir /app docker.io/library/golang:bullseye make bin/gitlab-runner

podman-remote: bin/podman-remote

bin/gitlab-runner: src/gitlab-runner FORCE
	cd $<; \
	go build -o $(CURDIR)/$@ \
	  .

bin/podman-remote: src/podman FORCE
	cd $<; \
	go build -o $(CURDIR)/$@ \
	  -tags "remote exclude_graphdriver_btrfs btrfs_noversion exclude_graphdriver_devicemapper containers_image_openpgp" \
	  ./cmd/podman

FORCE:
