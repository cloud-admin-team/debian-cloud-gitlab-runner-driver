# Debian Cloud GitLab Runner Driver

Implementation of a GitLab Runner Custom executor driver using qemu (kvm) and podman.
It is used by the Debian Cloud team for it's runner handling all the official builds.

All build steps are run
- in a privileged container and
- with the global `/dev` mounted for access `loop` devices.

## Setup

* Download a `genericcloud` Debian Bullseye image from https://cloud.debian.org/images/cloud/bullseye/.
* Register a runner, using `gitlab-runner register`, modify config to include:
  ```
  builds_dir = "/builds"
  cache_dir = "/cache"
  [runners.custom]
    config_exec  = "REPO/driver/config"
    prepare_exec = "REPO/driver/prepare"
    run_exec     = "REPO/driver/run"
    cleanup_exec = "REPO/driver/cleanup"
    prepare_args = ["REPO/config"]
    run_args     = ["REPO/config"]
    cleanup_args = ["REPO/config"]
  ```
* Setup the driver config in `REPO/config` (see `config.example`).
* Call `make` to build patched `gitlab-runner` and `podman-remote`.

## Limitations

* It only works with amd64 guests and requires kvm.
* Requires a patched `gitlab-runner` (see e.g. [gitlab-org/gitlab-runner!2911](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2911)).
* Requires a `podman-remote` or `podman` binary from a version smaller or equal the version in Bullseye (aka 3.0).
* Temporary storage of the VM is stored in a temporary file in the same directory as the input image.
