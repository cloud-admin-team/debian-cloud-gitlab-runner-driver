#!/bin/bash

set -euE
set -o pipefail

DRIVER=$(dirname $0)
source "${DRIVER}/.common.sh"
SCRIPT=$1
STAGE=$2

e=0

if [[ $STAGE == step_script ]] || [[ $STAGE == build_script ]] || [[ $STAGE == after_script ]]; then
  podman_remote run \
    --interactive \
    --privileged \
    --quiet \
    --net host \
    --volume builds:/builds \
    --volume cache:/cache \
    --volume /dev:/dev \
    ${CUSTOM_ENV_CI_JOB_IMAGE:-docker.io/library/debian:bullseye-slim} \
    sh -c "${BASH_DETECT}" < "$SCRIPT" || e=$?
else
  podman_remote run \
    --interactive \
    --privileged \
    --quiet \
    --net host \
    --volume builds:/builds \
    --volume cache:/cache \
    --volume /dev:/dev \
    registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:x86_64-latest \
    sh -c "${BASH_DETECT}" < <(echo 'ln -s gitlab-runner-helper /usr/bin/gitlab-runner;'; cat "$SCRIPT") || e=$?
fi

if [[ $e = 125 ]]; then
  exit $SYSTEM_FAILURE_EXIT_CODE
elif [[ $e != 0 ]]; then
  exit $BUILD_FAILURE_EXIT_CODE
fi
exit 0
