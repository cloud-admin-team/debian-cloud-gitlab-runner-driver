trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

CPU=1
MEM=4096
IP=127.255.255.0
HOSTNAME=runner-$CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN-project-$CUSTOM_ENV_CI_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID
PORT_REST=$(( $CUSTOM_ENV_CI_CONCURRENT_ID + 64768 ))
PORT_SSH=$(( $CUSTOM_ENV_CI_CONCURRENT_ID + 65024 ))
declare -a QEMU_EXTRA

declare -A _COLOR=(
  [bold_red]="\033[31;1m"
  [bold_green]="\033[32;1m"
  [yellow]="\033[0;33m"
  [reset]="\033[0;m"
)
function error() {
  echo -e "${_COLOR[bold_red]}ERROR: $*${_COLOR[reset]}" >&2
  exit "$SYSTEM_FAILURE_EXIT_CODE"
}
function warning() {
  echo -e "${_COLOR[yellow]}WARNING: $*${_COLOR[reset]}" >&2
}
function notice() {
  echo -e "${_COLOR[bold_green]}$*${_COLOR[reset]}"
}

ARG_CONFIG=$1
shift 1
source "$ARG_CONFIG"

NAME=gitlab-$HOSTNAME
SERVICE=$NAME.service

BASH_DETECT="
if [ -x /usr/local/bin/bash ]; then
  exec /usr/local/bin/bash
elif [ -x /usr/bin/bash ]; then
  exec /usr/bin/bash
elif [ -x /bin/bash ]; then
  exec /bin/bash
elif [ -x /usr/local/bin/sh ]; then
  exec /usr/local/bin/sh
elif [ -x /usr/bin/sh ]; then
  exec /usr/bin/sh
elif [ -x /bin/sh ]; then
  exec /bin/sh
elif [ -x /busybox/sh ]; then
  exec /busybox/sh
else
  echo shell not found
  exit 1
fi"

PODMAN=$(PATH=$(dirname $0)/../bin:$PATH command -v podman-remote podman | head -1 || :)

if [[ $PODMAN ]]; then
  podman_remote() {
    $PODMAN --url tcp://$IP:$PORT_REST --remote "$@"
  }
else
  echo 'Unable to find podman or podman-remote binarie' >&2
  exit $SYSTEM_FAILURE_EXIT_CODE
fi

if [[ ! -f $SSHAUTHORIZEDKEYS ]]; then
  error 'Unable to find ssh authorized keys file'
fi

cleanup() {
  systemctl \
    --user \
    stop \
    $SERVICE 2>/dev/null || :
}
